﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">Debug,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Config" Type="Folder">
			<Item Name="System Config.ini" Type="Document" URL="../../../Exe/Luxshare-OET/Config/System Config.ini"/>
		</Item>
		<Item Name="Libraries" Type="Folder">
			<Item Name="SPI EEPROM IC" Type="Folder">
				<Item Name="IS25LP080D.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp">
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="IS25LP080D.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp/IS25LP080D.lvclass"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/IS25LP080D.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
				</Item>
				<Item Name="M95M02-DR.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp">
					<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
					<Item Name="Application Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Application Directory.vi"/>
					<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
					<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
					<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/High Resolution Relative Seconds.vi"/>
					<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="M95M02-DR.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/M95M02-DR.lvclass"/>
					<Item Name="NI_FileType.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
					<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
					<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="subTimeDelay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
					<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
					<Item Name="Test Read.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/Tester/Test Read.vi"/>
					<Item Name="Test Write.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/Tester/Test Write.vi"/>
					<Item Name="Timer Session.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/NI/Timer/Timer Session.lvclass"/>
					<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
					<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/M95M02-DR.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
				</Item>
				<Item Name="MX25R4035F.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp">
					<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="MX25R4035F.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/MX25R4035F.lvclass"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
					<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
					<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
					<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/MX25R4035F.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
				</Item>
				<Item Name="SST25P040C.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/SST25P040C.lvlibp">
					<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/SST25P040C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/SST25P040C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/SST25P040C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/SST25P040C.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="SST25P040C.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/SST25P040C.lvlibp/SST25P040C.lvclass"/>
				</Item>
				<Item Name="W25Q80DV.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/W25Q80DV.lvlibp">
					<Item Name="LVOOP Get Default Object__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/W25Q80DV.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Get Default Object__ogtk.vi"/>
					<Item Name="LVOOP Return Class Name__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/W25Q80DV.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/LVOOP Return Class Name__ogtk.vi"/>
					<Item Name="Qualified Name Array To Single String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/W25Q80DV.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Qualified Name Array To Single String.vi"/>
					<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/W25Q80DV.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
					<Item Name="W25Q80DV.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/SPI EEPROM IC/W25Q80DV.lvlibp/W25Q80DV.lvclass"/>
				</Item>
			</Item>
			<Item Name="USB Communication" Type="Folder">
				<Item Name="USB-I2C" Type="Folder">
					<Item Name="USB-I2C.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp">
						<Item Name="Palette" Type="Folder">
							<Item Name="USB-I2C.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C.mnu"/>
							<Item Name="USB-I2C Example.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Example.mnu"/>
						</Item>
						<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
						<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
						<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
						<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
						<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
						<Item Name="Get File Extension.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
						<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
						<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
						<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
						<Item Name="Get PString__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
						<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
						<Item Name="MGI String Pattern Match Exists.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_MGI/String/MGI String Pattern Match Exists.vi"/>
						<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
						<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
						<Item Name="Recursive File List.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
						<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
						<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
						<Item Name="subTimeDelay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
						<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
						<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/user.lib/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
						<Item Name="USB-I2C Create.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C Create/USB-I2C Create.lvclass"/>
						<Item Name="USB-I2C.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/USB-I2C/USB-I2C.lvclass"/>
						<Item Name="VI Tree.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/VI Tree.vi"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-I2C/USB-I2C.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
				</Item>
				<Item Name="USB-SPI" Type="Folder">
					<Item Name="CP2130.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp">
						<Item Name="APIs" Type="Folder">
							<Item Name="CP213x Abort Input Pipe.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Abort Input Pipe.vi"/>
							<Item Name="CP213x Abort Output Pipe.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Abort Output Pipe.vi"/>
							<Item Name="CP213x Close.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Close.vi"/>
							<Item Name="CP213x Flush Input Pipe.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Flush Input Pipe.vi"/>
							<Item Name="CP213x Flush Output Pipe.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Flush Output Pipe.vi"/>
							<Item Name="CP213x Get Chip Select.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Chip Select.vi"/>
							<Item Name="CP213x Get Clock Divider.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Clock Divider.vi"/>
							<Item Name="CP213x Get Device Descriptor.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Device Descriptor.vi"/>
							<Item Name="CP213x Get Device Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Device Path.vi"/>
							<Item Name="CP213x Get Device Version.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Device Version.vi"/>
							<Item Name="CP213x Get Event Counter.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Event Counter.vi"/>
							<Item Name="CP213x Get Fifo Full Threshold.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Fifo Full Threshold.vi"/>
							<Item Name="CP213x Get Gpio Values.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Gpio Values.vi"/>
							<Item Name="CP213x Get Library Version.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Library Version.vi"/>
							<Item Name="CP213x Get Lock.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Lock.vi"/>
							<Item Name="CP213x Get Manufacturing String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Manufacturing String.vi"/>
							<Item Name="CP213x Get Num Devices.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Num Devices.vi"/>
							<Item Name="CP213x Get Opened Device Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Opened Device Path.vi"/>
							<Item Name="CP213x Get Opened Vid Pid.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Opened Vid Pid.vi"/>
							<Item Name="CP213x Get Pin Config.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Pin Config.vi"/>
							<Item Name="CP213x Get Product String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Product String.vi"/>
							<Item Name="CP213x Get Rtr State.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Rtr State.vi"/>
							<Item Name="CP213x Get Serial String.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Serial String.vi"/>
							<Item Name="CP213x Get Spi Control Bytes.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Spi Control Bytes.vi"/>
							<Item Name="CP213x Get Spi Delay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Spi Delay.vi"/>
							<Item Name="CP213x Get String Descriptor.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get String Descriptor.vi"/>
							<Item Name="CP213x Get Usb Config.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Usb Config.vi"/>
							<Item Name="CP213x Get Vid Pid.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Get Vid Pid.vi"/>
							<Item Name="CP213x Is Opened.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Is Opened.vi"/>
							<Item Name="CP213x Open By Index.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Open By Index.vi"/>
							<Item Name="CP213x Read Abort.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Read Abort.vi"/>
							<Item Name="CP213x Read Poll.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Read Poll.vi"/>
							<Item Name="CP213x Read Prom.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Read Prom.vi"/>
							<Item Name="CP213x Reset.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Reset.vi"/>
							<Item Name="CP213x Set Chip Select.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Chip Select.vi"/>
							<Item Name="CP213x Set Clock Divider.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Clock Divider.vi"/>
							<Item Name="CP213x Set Event Counter.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Event Counter.vi"/>
							<Item Name="CP213x Set Fifo Full Threshold.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Fifo Full Threshold.vi"/>
							<Item Name="CP213x Set Gpio Values.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Gpio Values.vi"/>
							<Item Name="CP213x Set Lock.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Lock.vi"/>
							<Item Name="CP213x Set Pin Config.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Pin Config.vi"/>
							<Item Name="CP213x Set Rtr Stop.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Rtr Stop.vi"/>
							<Item Name="CP213x Set Spi Control Byte.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Spi Control Byte.vi"/>
							<Item Name="CP213x Set Spi Delay.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Spi Delay.vi"/>
							<Item Name="CP213x Set Usb Config.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Set Usb Config.vi"/>
							<Item Name="CP213x Transfer Read Async.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Transfer Read Async.vi"/>
							<Item Name="CP213x Transfer Read Rtr Async.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Transfer Read Rtr Async.vi"/>
							<Item Name="CP213x Transfer Read Rtr Sync.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Transfer Read Rtr Sync.vi"/>
							<Item Name="CP213x Transfer Read Sync.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Transfer Read Sync.vi"/>
							<Item Name="CP213x Transfer Write Read.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Transfer Write Read.vi"/>
							<Item Name="CP213x Transfer Write.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Transfer Write.vi"/>
							<Item Name="CP213x Write Prom.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/APIs/CP213x Write Prom.vi"/>
						</Item>
						<Item Name="Control" Type="Folder">
							<Item Name="Chip Select Pin Mode-Enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Chip Select Pin Mode-Enum.ctl"/>
							<Item Name="Clock Frequency-Enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Clock Frequency-Enum.ctl"/>
							<Item Name="Clock Phase (CPHA)-Enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Clock Phase (CPHA)-Enum.ctl"/>
							<Item Name="Clock Polarity (CPOL)-Enum.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Clock Polarity (CPOL)-Enum.ctl"/>
							<Item Name="Get Spi Delay-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Get Spi Delay-Cluster.ctl"/>
							<Item Name="Get USB Config-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Get USB Config-Cluster.ctl"/>
							<Item Name="Set Spi Delay-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Set Spi Delay-Cluster.ctl"/>
							<Item Name="Set USB Config-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Set USB Config-Cluster.ctl"/>
							<Item Name="Transfer Read Rtr Sync Setting-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Transfer Read Rtr Sync Setting-Cluster.ctl"/>
							<Item Name="Transfer Read Sync Setting-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Transfer Read Sync Setting-Cluster.ctl"/>
							<Item Name="Transfer Write Read Setting-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Transfer Write Read Setting-Cluster.ctl"/>
							<Item Name="Transfer Write Setting-Cluster.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Control/Transfer Write Setting-Cluster.ctl"/>
						</Item>
						<Item Name="Palette Menus" Type="Folder">
							<Item Name="CP2130-Configure.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Palette Menus/CP2130-Configure.mnu"/>
							<Item Name="CP2130.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Palette Menus/CP2130.mnu"/>
							<Item Name="USB-SPI.mnu" Type="Document" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Palette Menus/USB-SPI.mnu"/>
						</Item>
						<Item Name="Private" Type="Folder">
							<Item Name="Error Converter (ErrCode or Status).vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Private/Error Converter (ErrCode or Status).vi"/>
							<Item Name="Get Dll Path.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/Private/Get Dll Path.vi"/>
						</Item>
						<Item Name="CP2130.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/CP2130.lvclass"/>
						<Item Name="Get System Directory.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/Get System Directory.vi"/>
						<Item Name="List Directory and LLBs.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/List Directory and LLBs.vi"/>
						<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/menus/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
						<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
						<Item Name="Recursive File List.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Recursive File List.vi"/>
						<Item Name="Space Constant.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
						<Item Name="System Directory Type.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/sysdir.llb/System Directory Type.ctl"/>
						<Item Name="Trim Whitespace.vi" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
						<Item Name="whitespace.ctl" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/CP2130.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
					</Item>
					<Item Name="MCU Direct RW.lvlibp" Type="LVLibp" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/MCU Direct RW.lvlibp">
						<Item Name="MCU Direct RW.lvclass" Type="LVClass" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/MCU Direct RW.lvlibp/MCU Direct RW.lvclass"/>
						<Item Name="Stall Data Flow.vim" Type="VI" URL="../../../Exe/Luxshare-OET/Libraries/USB Communication/USB-SPI/MCU Direct RW.lvlibp/1abvi3w/vi.lib/Utility/Stall Data Flow.vim"/>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="Message Queue Logger.vi" Type="VI" URL="../SPI EEPROM/Message Queue Logger.vi"/>
		</Item>
		<Item Name="SPI EEPROM.lvlib" Type="Library" URL="../SPI EEPROM/SPI EEPROM.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="1D Array to String__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/1D Array to String__ogtk.vi"/>
				<Item Name="AES Algorithm.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Algorithm.vi"/>
				<Item Name="AES Keygenerator.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Keygenerator.vi"/>
				<Item Name="AES poly mult.vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES poly mult.vi"/>
				<Item Name="AES Rounds .vi" Type="VI" URL="/&lt;userlib&gt;/Crypto-Tools/Crypto.llb/AES Rounds .vi"/>
				<Item Name="Array of VData to VCluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Array of VData to VCluster__ogtk.vi"/>
				<Item Name="Build Error Cluster__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/error/error.llb/Build Error Cluster__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays - path__ogtk.vi"/>
				<Item Name="Build Path - File Names and Paths Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names and Paths Arrays__ogtk.vi"/>
				<Item Name="Build Path - File Names Array - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array - path__ogtk.vi"/>
				<Item Name="Build Path - File Names Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - File Names Array__ogtk.vi"/>
				<Item Name="Build Path - Traditional - path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional - path__ogtk.vi"/>
				<Item Name="Build Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path - Traditional__ogtk.vi"/>
				<Item Name="Build Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Build Path__ogtk.vi"/>
				<Item Name="ClassID Names Enum__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/ClassID Names Enum__ogtk.ctl"/>
				<Item Name="Cluster to Array of VData__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Cluster to Array of VData__ogtk.vi"/>
				<Item Name="Create Dir if Non-Existant__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Create Dir if Non-Existant__ogtk.vi"/>
				<Item Name="Current VIs Parents Ref__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/appcontrol/appcontrol.llb/Current VIs Parents Ref__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Boolean)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (I64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (LVObject)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Path)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (String)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U8)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U16)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U32)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (U64)__ogtk.vi"/>
				<Item Name="Delete Elements from 2D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from 2D Array (Variant)__ogtk.vi"/>
				<Item Name="Delete Elements from Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Delete Elements from Array__ogtk.vi"/>
				<Item Name="End of Line Constant (bug fix).vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/End of Line Constant (bug fix).vi"/>
				<Item Name="Extract Window Names.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Extract Window Names.vi"/>
				<Item Name="Filter 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Boolean)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CDB)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CSG)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (CXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (DBL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (EXT)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (I64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (LVObject)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Path)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (SGL)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (String)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U8)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U16)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U32)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (U64)__ogtk.vi"/>
				<Item Name="Filter 1D Array with Scalar (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array with Scalar (Variant)__ogtk.vi"/>
				<Item Name="Filter 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Filter 1D Array__ogtk.vi"/>
				<Item Name="Get Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element by Name__ogtk.vi"/>
				<Item Name="Get Cluster Element Names__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Element Names__ogtk.vi"/>
				<Item Name="Get Cluster Elements TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Cluster Elements TDs__ogtk.vi"/>
				<Item Name="Get Data Name from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name from TD__ogtk.vi"/>
				<Item Name="Get Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Data Name__ogtk.vi"/>
				<Item Name="Get Header from TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Header from TD__ogtk.vi"/>
				<Item Name="Get Last PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Last PString__ogtk.vi"/>
				<Item Name="Get LVWUtil32.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/SubVIs/Get LVWUtil32.vi"/>
				<Item Name="Get PString__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get PString__ogtk.vi"/>
				<Item Name="Get Task List.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Get Task List.vi"/>
				<Item Name="Get TDEnum from Data__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get TDEnum from Data__ogtk.vi"/>
				<Item Name="Get Variant Attributes__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Get Variant Attributes__ogtk.vi"/>
				<Item Name="Get Window RefNum.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Get Window RefNum.vi"/>
				<Item Name="List Directory Recursive__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory Recursive__ogtk.vi"/>
				<Item Name="List Directory__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/List Directory__ogtk.vi"/>
				<Item Name="Make Window Always on Top.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Make Window Always on Top.vi"/>
				<Item Name="Maximize Window.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Maximize Window.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI Get VI Control Ref[].vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Get VI Control Ref[].vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
				<Item Name="Not a Window Refnum" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Not a Window Refnum"/>
				<Item Name="Parse String with TDs__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Parse String with TDs__ogtk.vi"/>
				<Item Name="PostMessage Master.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/PostMessage Master.vi"/>
				<Item Name="Quit Application.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/Quit Application.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (I64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Path)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (String)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U8)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U16)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U32)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (U64)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Remove Duplicates from 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Remove Duplicates from 1D Array__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 1D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 1D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Boolean)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CDB)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CSG)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (CXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (DBL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (EXT)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (I64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (LVObject)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Path)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (SGL)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (String)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U8)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U16)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U32)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (U64)__ogtk.vi"/>
				<Item Name="Reorder 2D Array2 (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder 2D Array2 (Variant)__ogtk.vi"/>
				<Item Name="Reorder Array2__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Reorder Array2__ogtk.vi"/>
				<Item Name="Search 1D Array (Boolean)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Boolean)__ogtk.vi"/>
				<Item Name="Search 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Search 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Search 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Search 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Search 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I8)__ogtk.vi"/>
				<Item Name="Search 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I16)__ogtk.vi"/>
				<Item Name="Search 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I32)__ogtk.vi"/>
				<Item Name="Search 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (I64)__ogtk.vi"/>
				<Item Name="Search 1D Array (LVObject)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (LVObject)__ogtk.vi"/>
				<Item Name="Search 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Path)__ogtk.vi"/>
				<Item Name="Search 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Search 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (String)__ogtk.vi"/>
				<Item Name="Search 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U8)__ogtk.vi"/>
				<Item Name="Search 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U16)__ogtk.vi"/>
				<Item Name="Search 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U32)__ogtk.vi"/>
				<Item Name="Search 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (U64)__ogtk.vi"/>
				<Item Name="Search 1D Array (Variant)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search 1D Array (Variant)__ogtk.vi"/>
				<Item Name="Search Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Search Array__ogtk.vi"/>
				<Item Name="Set Cluster Element by Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Cluster Element by Name__ogtk.vi"/>
				<Item Name="Set Data Name__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Set Data Name__ogtk.vi"/>
				<Item Name="Set Window Z-Position.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Set Window Z-Position.vi"/>
				<Item Name="Show Window.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Show Window.vi"/>
				<Item Name="Sort 1D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 1D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 1D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 1D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 1D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (String)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 1D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 1D Array (U64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CDB)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CDB)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CSG)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CSG)__ogtk.vi"/>
				<Item Name="Sort 2D Array (CXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (CXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (DBL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (DBL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (EXT)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (EXT)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (I64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (I64)__ogtk.vi"/>
				<Item Name="Sort 2D Array (Path)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (Path)__ogtk.vi"/>
				<Item Name="Sort 2D Array (SGL)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (SGL)__ogtk.vi"/>
				<Item Name="Sort 2D Array (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (String)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U8)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U8)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U16)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U16)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U32)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U32)__ogtk.vi"/>
				<Item Name="Sort 2D Array (U64)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort 2D Array (U64)__ogtk.vi"/>
				<Item Name="Sort Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/array/array.llb/Sort Array__ogtk.vi"/>
				<Item Name="Split Cluster TD__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Split Cluster TD__ogtk.vi"/>
				<Item Name="String to 1D Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/String to 1D Array__ogtk.vi"/>
				<Item Name="Strip Path - Arrays__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Arrays__ogtk.vi"/>
				<Item Name="Strip Path - Traditional__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path - Traditional__ogtk.vi"/>
				<Item Name="Strip Path__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/Strip Path__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Type Descriptor Enumeration__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Enumeration__ogtk.ctl"/>
				<Item Name="Type Descriptor Header__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor Header__ogtk.ctl"/>
				<Item Name="Type Descriptor__ogtk.ctl" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Type Descriptor__ogtk.ctl"/>
				<Item Name="Variant to Header Info__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/lvdata/lvdata.llb/Variant to Header Info__ogtk.vi"/>
				<Item Name="Window Refnum" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/WINUTIL.LLB/Window Refnum"/>
				<Item Name="WinUtil Master.vi" Type="VI" URL="/&lt;userlib&gt;/NI WinAPI/Winevent.llb/WinUtil Master.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="_ExponentialCalculation__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_ExponentialCalculation__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(I32)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(I32)__lava_lib_ui_tools.vi"/>
				<Item Name="_Fade(String)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_Fade(String)__lava_lib_ui_tools.vi"/>
				<Item Name="_fadetype__lava_lib_ui_tools.ctl" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/_fadetype__lava_lib_ui_tools.ctl"/>
				<Item Name="AddNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/AddNamedRendezvousPrefix.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create New Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create New Rendezvous.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Delacor_lib_QMH_Cloneable Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Cloneable Module Admin_class/Delacor_lib_QMH_Cloneable Module Admin.lvclass"/>
				<Item Name="Delacor_lib_QMH_Message Queue.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Message Queue_class/Delacor_lib_QMH_Message Queue.lvclass"/>
				<Item Name="Delacor_lib_QMH_Module Admin.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Delacor/Delacor QMH/Libraries/Module Admin_class/Delacor_lib_QMH_Module Admin.lvclass"/>
				<Item Name="Destroy A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy A Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Fade (Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade (Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="Fade__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/Fade__lava_lib_ui_tools.vi"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get VI Library File Info.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get VI Library File Info.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/GetNamedRendezvousPrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GPMath.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Math/GPMath.lvlib"/>
				<Item Name="GPString.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/String/GPString.lvlib"/>
				<Item Name="GPTiming.lvlib" Type="Library" URL="/&lt;vilib&gt;/GPower/Timing/GPTiming.lvlib"/>
				<Item Name="Has LLB Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Has LLB Extension.vi"/>
				<Item Name="Librarian File Info In.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info In.ctl"/>
				<Item Name="Librarian File Info Out.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File Info Out.ctl"/>
				<Item Name="Librarian File List.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian File List.ctl"/>
				<Item Name="Librarian.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Librarian.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Not A Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Not A Rendezvous.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Read Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Read Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (I64).vi"/>
				<Item Name="Read Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet (string).vi"/>
				<Item Name="Read Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Delimited Spreadsheet.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read Lines From File (with error IO).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File (with error IO).vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Rectangle Size__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Alignment/Rectangle Size__lava_lib_ui_tools.vi"/>
				<Item Name="RectSize.vi" Type="VI" URL="/&lt;vilib&gt;/picture/PictureSupport.llb/RectSize.vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Waiting Procs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Release Waiting Procs.vi"/>
				<Item Name="RemoveNamedRendezvousPrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RemoveNamedRendezvousPrefix.vi"/>
				<Item Name="Rendezvous Name &amp; Ref DB Action.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB Action.ctl"/>
				<Item Name="Rendezvous Name &amp; Ref DB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous Name &amp; Ref DB.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="RendezvousDataCluster.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/RendezvousDataCluster.ctl"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="SetTransparency(Polymorphic)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(Polymorphic)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency(U8)__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency(U8)__lava_lib_ui_tools.vi"/>
				<Item Name="SetTransparency__lava_lib_ui_tools.vi" Type="VI" URL="/&lt;vilib&gt;/LAVA/UI Tools/Fading/SetTransparency__lava_lib_ui_tools.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Find Search Mode.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Find Search Mode.ctl"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Delimited Spreadsheet (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (DBL).vi"/>
				<Item Name="Write Delimited Spreadsheet (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (I64).vi"/>
				<Item Name="Write Delimited Spreadsheet (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet (string).vi"/>
				<Item Name="Write Delimited Spreadsheet.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Delimited Spreadsheet.vi"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Authorization.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/Authorization.lvlib"/>
			<Item Name="Control Refnum.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/Control Refnum/Control Refnum.lvlib"/>
			<Item Name="Encrypt &amp; Decrypt String.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Encrypt &amp; Decrypt String.vi"/>
			<Item Name="Get BIOS Info.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get BIOS Info.vi"/>
			<Item Name="Get Key.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get Key.vi"/>
			<Item Name="Get Volume Info.vi" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/SubVIs/Get Volume Info.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Luxshare-OET API.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Luxshare-OET API.lvlib"/>
			<Item Name="Mode-Enum.ctl" Type="VI" URL="/&lt;menus&gt;/Categories/Luxshare-OET/Authorization/Control/Mode-Enum.ctl"/>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="System Exec.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/System Exec/System Exec.lvlib"/>
			<Item Name="System.Management" Type="Document" URL="System.Management">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Update EEPROM By I2C Argument--cluster.ctl" Type="VI" URL="../SPI EEPROM/Update EEPROM By I2C Argument--cluster.ctl"/>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="VI.lvlib" Type="Library" URL="/&lt;menus&gt;/Categories/User Library/Application Control/VI Refnum/VI.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="SPI EEPROM" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{28B6B447-272B-4FFE-B23B-6662E0A951DC}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">V1.18.0-20241104
1.Add IS25LP080D and MxL9364x

V1.17.0-20240719
1.AN8918 Add I2C Select

V1.16.0-20230703
1.87400 Add I2C Select

V1.15.2-20230328
1.Get DSP Type Add new items

V1.15.1-20230203
1.AN8911 Add I2C Write

V1.15.0-20230130
1.Add New EEPROM W25Q80DV

V1.14.1-20221206
1.8784x - SST Add I2C Write Select

V1.14.0-20220830
1.8784x Add SST and MX Select

V1.13.0-20220719
1.8780x Add SST and MX Select

V1.12.0-20220627
1.I2C Write Add 87580

V1.11.0-20220617
1.Add Support Multi Update By I2C
2.8740x Add MX version

V1.10.1-20211209
1.Public API Type Define add 8758x MX and SST Enum Type Define Value

V1.10.0-20211208
1.8758x Add MX to write

V1.9.0-20211206
1.8754x-MX Add I2C Write Function

V1.8.0-20211202
1.8754x-SST Add I2C Write Function

V1.7.0-20210512
1.Now Support Dynamic Call

V1.6.0-20210119
1.Add MX25R4035F EEPROM

V1.5.0-20201026
1.Add Voltage Level to show eeprom use voltage

V1.4.1-20200708
1.Add auto select IC class by DSP
2.Check EEPROM State Change use Check State

V1.4.0-20200706
1.DSP Ring change to auto load dsp file name
2.Add Path Pattern By DSP Ring Select

V1.3.2-20191012
1.PPL Link Change
2.Do validation by DQMH tool

V1.3.1-20190818
1.Open File Format Changed

V1.3.0-20190814
1.Add Help Button

V1.2.2-20190729
1.Disable USB-I2C control

V1.2.1-20190725
1.Disable IO Control function

V1.2.0-20190510
1.Disable IC Select
2.Add DSP item for select

V1.1.0-20190226
1.Add Check EEPROM State After Configure

V1.0.2-20181212
1.Hex File Transfer Change to Bin File

V1.0.1-20181205
1.Fix Error Status display bug

V1.0.0</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SPI EEPROM</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B6541439-E732-428E-958F-8F4719E88934}</Property>
				<Property Name="Bld_version.build" Type="Int">39</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Bld_version.minor" Type="Int">17</Property>
				<Property Name="Destination[0].destName" Type="Str">SPI EEPROM.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules/NI_AB_PROJECTNAME.lvlibp</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/E/Luxshare-OET/Exe/Luxshare-OET/Modules</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{4F3D42D6-D57D-46DA-B395-E990C8C339C5}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/SPI EEPROM.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/SubVIs</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_companyName" Type="Str">Luxshare-Tech</Property>
				<Property Name="TgtF_fileDescription" Type="Str">PAM4 DSP EEPROM</Property>
				<Property Name="TgtF_internalName" Type="Str">SPI EEPROM</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright (c) 2017-2024 Luxshare-Tech Corporation. All rights reserved</Property>
				<Property Name="TgtF_productName" Type="Str">SPI EEPROM</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F80FBC05-C831-4666-97F2-75D7D03C7B7F}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">SPI EEPROM.lvlibp</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
